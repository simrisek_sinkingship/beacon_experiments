﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Handles saving and loading beacon configurations
 */
public static class Configurations : object {

    public static string currentConfigName
    {
        get
        {
            return PlayerPrefs.GetString("CurrentConfig", "Default");
        }

        set
        {
            PlayerPrefs.SetString("CurrentConfig", value);
            string[] knownConfigs = AllConfigNames();
            bool alreadyKnown = false;
            foreach(string c in knownConfigs)
            {
                if(c == value)
                {
                    alreadyKnown = true;
                }
            }
            if (!alreadyKnown)
            {
                PlayerPrefs.SetString("configList", string.Join("|", knownConfigs)+"|"+value);
            }
        }
    }


    public static string[] AllConfigNames()
    {
        return PlayerPrefs.GetString("configList").Split('|');
    }

    public static string[] LoadCurrent()
    {
        return Load(currentConfigName);
    }

    public static string[] Load(string configName)
    {
        string[] beaconStrings = PlayerPrefs.GetString("beaconList_"+configName).Split('|');
        return beaconStrings;
    }

    public static void Save(List<BeaconWrapper> beacons)
    {
        string listString = "";
        foreach (BeaconWrapper b in beacons)
        {
            BeaconDef bd = new BeaconDef(b.major, b.minor, b.pos);
            string bString = JsonUtility.ToJson(bd);
            if (listString == "")
            {
                listString += bString;
            }
            else
            {
                listString += "|" + bString;
            }
        }
        PlayerPrefs.SetString("beaconList_"+ currentConfigName, listString);
    }

}
