﻿using UnityEngine;
using OMobile.EstimoteUnity;

[System.Serializable]
public class BeaconDef
{
    [SerializeField]
    private int _major;
    [SerializeField]
    private int _minor;
    [SerializeField]
    private float _x;
    [SerializeField]
    private float _y;
    [SerializeField]
    private float _z;

    public int major {
        get { return _major; }
        set { _major = value; }
    }
    public int minor
    {
        get { return _minor; }
        set { _minor = value; }
    }
    public float x
    {
        get { return _x; }
        set { _x = value; }
    }
    public float y
    {
        get { return _y; }
        set { _y = value; }
    }
    public float z
    {
        get { return _z; }
        set { _z = value; }
    }


    public BeaconDef(int bmajor, int bminor, Vector3 bpos)
    {
        _major = bmajor;
        _minor = bminor;
        _x = bpos.x;
        _y = bpos.y;
        _z = bpos.z;
    }
}


public class BeaconWrapper {

    public EstimoteUnityBeacon beacon;
    [SerializeField]
    private int _major;
    [SerializeField]
    private int _minor;
    [SerializeField]
    private Vector3 _pos;
    [SerializeField]
    private GameObject _txtDisplay;
    [SerializeField]
    private GameObject _visDisplay;

    public int major
    {
        get { return _major; }
        set { _major = value; }
    }
    public int minor
    {
        get { return _minor; }
        set { _minor = value; }
    }

    public Vector3 pos
    {
        get { return _pos; }
        set { _pos = value; }
    }
    public GameObject txtDisplay
    {
        get { return _txtDisplay; }
        set { _txtDisplay = value; }
    }

    public GameObject visDisplay
    {
        get { return _visDisplay; }
        set { _visDisplay = value; }
    }

    private Color ON_COL = Color.green;
    private Color OFF_COL = Color.black;
    private Color UNKNOWN = new Color(0, 150, 0);

    public BeaconWrapper(int maj, int min, Vector3 pos, GameObject td, GameObject vd)
    {
        _major = maj;
        _minor = min;
        _pos = pos;
        _txtDisplay = td;
        _visDisplay = vd;

    }

    public void SetX(float x)
    {
        _pos = new Vector3(x, _pos.y, _pos.z);
    }

    public void SetY(float y)
    {
        _pos = new Vector3(_pos.x, y, _pos.z);
    }

    public void SetZ(float z)
    {
        _pos = new Vector3(_pos.x, _pos.y, z);
    }

    public void UpdateLight(){
        if (beacon == null)
        {
            _txtDisplay.transform.Find("light").GetComponent<SpriteRenderer>().color = OFF_COL;
        }
        else
        {
            System.TimeSpan sinceSeen = System.DateTime.Now - beacon.LastSeen;
            if (sinceSeen.Seconds > 30)
            {
                _txtDisplay.transform.Find("light").GetComponent<SpriteRenderer>().color = UNKNOWN;
            }
            else
            {
                _txtDisplay.transform.Find("light").GetComponent<SpriteRenderer>().color = ON_COL;
            }
        }
    }
}


