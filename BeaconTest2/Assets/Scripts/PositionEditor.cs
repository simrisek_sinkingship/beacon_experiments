﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class PositionEditor : MonoBehaviour {

    public Transform _beaconsTransform;
    public Transform _buttonsTransform;
    public GameObject _addBeaconPopup;
    public GameObject _editBeaconPopup;
    public GameObject _newConfigPopup;
    public Button _addButton;
    public Button _doneButton;
    public Button _loadButton;
    public Button _newButton;

    public GameObject _beaconDisplayPrefab;
    public GameObject _configDisplayPrefab;
    public OMobile.EstimoteUnity.EstimoteUnity _estimote;
    public OMobile.EstimoteUnity.Demo.EstimoteUnityDemo _eud;
    public Text _configNameDisplay;

    private BeaconWrapper _beaconUnderEdit;

    private string _configName = "Default";

    private List<BeaconWrapper> _knownBeacons;

    private float BEACON_SPACING = 140f;
 

    private void Awake()
    {
        _estimote.Initialize();
    }
    // Use this for initialization
    void Start () {

        _configName = Configurations.currentConfigName;
        _configNameDisplay.text = _configName;
        _knownBeacons = new List<BeaconWrapper>();
        _addBeaconPopup.SetActive(false);
        _editBeaconPopup.SetActive(false);

        LoadSavedData();

        _estimote.StartScanning();


    }
	
	// Update is called once per frame
	void Update () {
        foreach (BeaconWrapper b in _knownBeacons)
        {
            b.UpdateLight();

            List<OMobile.EstimoteUnity.EstimoteUnityBeacon> detecteds = _eud.getDetectedBeacons();
            foreach(OMobile.EstimoteUnity.EstimoteUnityBeacon db in detecteds)
            {
                if(db.Minor == b.minor)
                {
                    b.beacon = db;
                }
            }
        }
    }

    public void LoadSavedData()
    {
        string[] beaconStrings = Configurations.Load(_configName);

        foreach(string s in beaconStrings)
        {
            Debug.Log(s);
            BeaconDef bd = JsonUtility.FromJson<BeaconDef>(s);
            Debug.Log(bd);
            if(bd != null)
            {
                AddSavedBeacon(bd);
            }
        }
    }

    public void AddBeacon()
    {
        int major = int.Parse(_addBeaconPopup.transform.Find("text_major").GetComponent<InputField>().text);
        int minor = int.Parse(_addBeaconPopup.transform.Find("text_minor").GetComponent<InputField>().text);
        float x = float.Parse(_addBeaconPopup.transform.Find("text_x").GetComponent<InputField>().text);
        float y = float.Parse(_addBeaconPopup.transform.Find("text_y").GetComponent<InputField>().text);
        float z = float.Parse(_addBeaconPopup.transform.Find("text_z").GetComponent<InputField>().text);

        GameObject newBD = NewBeaconDisplay(major, minor, new Vector3(x, y, z));
        BeaconWrapper newBW = new BeaconWrapper(major, minor, new Vector3(x, y, z), newBD, null);
          
        _knownBeacons.Add(newBW);
        Configurations.Save(_knownBeacons);

        _addBeaconPopup.SetActive(false);
    }

    private GameObject NewBeaconDisplay(int major, int minor, Vector3 pos)
    {
        GameObject newBD = Instantiate(_beaconDisplayPrefab);
        Vector3 displayPos = newBD.transform.position;
        newBD.transform.parent = _beaconsTransform;
        newBD.transform.localScale = 2.2f*Vector3.one;
        newBD.transform.localPosition = new Vector3(0, 0 - _knownBeacons.Count * BEACON_SPACING, displayPos.z);
        newBD.transform.Find("Button").Find("DisplayText").GetComponent<Text>().text = major+"  /  "+minor+ "\n" + pos.x.ToString() + ", " + pos.y.ToString() + ", " + pos.z.ToString();
        newBD.name = minor.ToString();
        newBD.transform.Find("Button").GetComponent<Button>().onClick.AddListener(OpenEdit);
    

        Debug.Log("created new display gameobject " + newBD);
        return newBD;
    }

    private void RedrawBeaconDisplays()
    {
        for(int i=0; i<_knownBeacons.Count; i++)
        {
            Vector3 bPos = _knownBeacons[i].txtDisplay.transform.localPosition;
            _knownBeacons[i].txtDisplay.transform.localPosition = new Vector3(520, 510 - i * BEACON_SPACING, bPos.z);
        }
    }

    public void AddSavedBeacon(BeaconDef bd)
    {
        int major = bd.major;
        int minor = bd.minor;
        
        float x = bd.x;
        float y = bd.y;
        float z = bd.z;
        GameObject newBD = NewBeaconDisplay(major, minor, new Vector3(x, y, z));
        BeaconWrapper newBW = new BeaconWrapper(major, minor, new Vector3(x, y, z), newBD, null);
        _knownBeacons.Add(newBW);
    }

    public void EditBeacon()
    {
        int major = int.Parse(_editBeaconPopup.transform.Find("text_major").GetComponent<InputField>().text);
        int minor = int.Parse(_editBeaconPopup.transform.Find("text_minor").GetComponent<InputField>().text);
        float x = float.Parse(_editBeaconPopup.transform.Find("text_x").GetComponent<InputField>().text);
        float y = float.Parse(_editBeaconPopup.transform.Find("text_y").GetComponent<InputField>().text);
        float z = float.Parse(_editBeaconPopup.transform.Find("text_z").GetComponent<InputField>().text);
        _beaconUnderEdit.major = major;
        _beaconUnderEdit.minor = minor;
        _beaconUnderEdit.pos = new Vector3(x, y, z);
        UpdateDisplay(_beaconUnderEdit);
        _editBeaconPopup.SetActive(false);
        Configurations.Save(_knownBeacons);
    }

    public void DeleteBeacon()
    {
        Destroy(_beaconUnderEdit.txtDisplay);
        _knownBeacons.Remove(_beaconUnderEdit);
        RedrawBeaconDisplays();
        _editBeaconPopup.SetActive(false);
        Configurations.Save(_knownBeacons);
    }

    public void ClearBeacons()
    {
        foreach (Transform child in _beaconsTransform)
        {
            Destroy(child.gameObject);
        }
        _knownBeacons = new List<BeaconWrapper>();
    }

    public void ClearButtons()
    {
        foreach (Transform child in _buttonsTransform)
        {
            Destroy(child.gameObject);
        }
    }

    private void UpdateDisplay(BeaconWrapper b)
    {
        Vector3 pos = b.pos;
        b.txtDisplay.transform.Find("Button").Find("DisplayText").GetComponent<Text>().text = b.major + "  /  " + b.minor + "\n" + pos.x.ToString() + ", " + pos.y.ToString() + ", " + pos.z.ToString();
        b.txtDisplay.name = b.minor.ToString();
    }

    public void OpenEdit()
    {
        string selectedName = EventSystem.current.currentSelectedGameObject.transform.parent.gameObject.name;
        
        foreach(BeaconWrapper b in _knownBeacons)
        {
            if(b.minor.ToString() == selectedName)
            {
                _beaconUnderEdit = b;
            }
        }
        if(_beaconUnderEdit == null)
        {
            Debug.Log("failed to identify beacon to edit.");
        }

        _addBeaconPopup.SetActive(false);
        _newConfigPopup.SetActive(false);
        _editBeaconPopup.SetActive(true);
        _addButton.gameObject.SetActive(true);

        _editBeaconPopup.transform.Find("text_major").GetComponent<InputField>().text = _beaconUnderEdit.major.ToString();
        _editBeaconPopup.transform.Find("text_minor").GetComponent<InputField>().text = _beaconUnderEdit.minor.ToString();
        _editBeaconPopup.transform.Find("text_x").GetComponent<InputField>().text = _beaconUnderEdit.pos.x.ToString();
        _editBeaconPopup.transform.Find("text_y").GetComponent<InputField>().text = _beaconUnderEdit.pos.y.ToString();
        _editBeaconPopup.transform.Find("text_z").GetComponent<InputField>().text = _beaconUnderEdit.pos.z.ToString();
    }

    public void OpenAdd()
    {
        _newConfigPopup.SetActive(false);
        _editBeaconPopup.SetActive(false);
        _addBeaconPopup.SetActive(true);
        _addButton.gameObject.SetActive(true);

        _addBeaconPopup.transform.Find("text_major").GetComponent<InputField>().text = "";
        _addBeaconPopup.transform.Find("text_minor").GetComponent<InputField>().text = "";
        _addBeaconPopup.transform.Find("text_x").GetComponent<InputField>().text = "";
        _addBeaconPopup.transform.Find("text_y").GetComponent<InputField>().text = "";
        _addBeaconPopup.transform.Find("text_z").GetComponent<InputField>().text = "";
    }

    public void OpenNew()
    {
        _addBeaconPopup.SetActive(false);
        _editBeaconPopup.SetActive(false);
        _newConfigPopup.SetActive(true);
    }

    public void NewConfig()
    {
        _configNameDisplay.text = _newConfigPopup.transform.Find("text_name").GetComponent<InputField>().text;
        Configurations.currentConfigName = _configNameDisplay.text;
        Configurations.Save(_knownBeacons);
        _newConfigPopup.SetActive(false);
    }

    public void OpenLoad()
    {
        _addButton.gameObject.SetActive(false);
        _configNameDisplay.gameObject.SetActive(false);
        ClearBeacons();
        string[] allConfigs = Configurations.AllConfigNames();
        for(int i=0; i<allConfigs.Length; i++)
        {
            GameObject cf = Instantiate(_configDisplayPrefab);
            cf.transform.parent = _buttonsTransform;
            cf.transform.localScale = 2.6f * Vector3.one;
            cf.transform.localPosition = new Vector3(0, 0 - i * BEACON_SPACING, 0);
            cf.transform.Find("button text").GetComponent<Text>().text = allConfigs[i];
            cf.transform.GetComponent<Button>().onClick.AddListener(LoadConfig);
        }

    }

    public void LoadConfig()
    {
        ClearButtons();
        _configName = EventSystem.current.currentSelectedGameObject.transform.Find("button text").GetComponent<Text>().text;
        _configNameDisplay.gameObject.SetActive(true);
        _addButton.gameObject.SetActive(true);
        _configNameDisplay.text = _configName;
        LoadSavedData();
    }

    public void Advance()
    {
        Configurations.Save(_knownBeacons);
        SceneManager.LoadScene("Trilateration");
    }

}
