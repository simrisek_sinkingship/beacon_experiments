﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


// 
// So the original idea was to heat map signal strength across a grid of pixels, and we may yet do that
// though we're not actually doing that yet.
//
 
public class Triangulator : MonoBehaviour {

    public int width;
    public int height;
    public Transform square;
    public List<Transform> _icons;
    public List<Transform> _cards;
    public Transform _frontCanvas;
    public Transform _backCanvas;


    public Transform guessPt;
    public Transform targetArea;
    public GameObject beaconIconPrefab;
    public GameObject beaconCardPrefab;

    private List<BeaconWrapper> _knownBeacons;
    private List<float[]> _recentReadings;
    private Transform[,] _pixels;
    private List<LineRenderer> lineRenderers;
    private List<LineRenderer> signalLRs;

    public OMobile.EstimoteUnity.EstimoteUnity _estimote;
    public OMobile.EstimoteUnity.Demo.EstimoteUnityDemo _eud;

    private int _mode = 0;

    private static float PIXEL_SCALE = 0.1f;
    private static int NUM_READINGS = 10;
    private float[,] _readings_acc;
    private float[,] _readings_rssi;
    private int _readingIndex = 0;

    private void Awake() {

        _knownBeacons = new List<BeaconWrapper>();
        lineRenderers = new List<LineRenderer>();
        signalLRs = new List<LineRenderer>();

        DisplaySavedBeacons();

       _readings_acc = new float[NUM_READINGS, _knownBeacons.Count];
       _readings_rssi = new float[NUM_READINGS, _knownBeacons.Count];

        targetArea.gameObject.SetActive(false);
 
    }

    void Start () {
        RenderGrid();  
	}


    private void DisplaySavedBeacons()
    {
        string[] _iconstrings = Configurations.LoadCurrent();

        float[,] colorvals = new float[,] { { 1, 0.2f, 0.5f }, { 0.5f, 1, 0.2f }, { 0.2f, 0.5f, 1 }, { 1, 1, 0 }, { 0, 1, 1 }, { 1, 0, 1 } };


        for (int i=0; i<_iconstrings.Length; i++)
        {
            BeaconDef bd = JsonUtility.FromJson<BeaconDef>(_iconstrings[i]);
            if (bd != null)
            {
                GameObject bi = Instantiate(beaconIconPrefab);
                bi.name = bd.minor.ToString();
                // transposing 3d data into an aerial view of the _icons
                bi.transform.position = new Vector3(bd.x, bd.z, -0.5f);
                
                Color iconCol = new Color(colorvals[i,0], colorvals[i,1], colorvals[i,2], 0.4f);
                Color iconColLowAlpha = iconCol;
                iconColLowAlpha.a = 0.2f;

                bi.transform.GetComponent<SpriteRenderer>().color = iconCol;
                bi.transform.Find("Signal").GetComponent<SpriteRenderer>().color = iconColLowAlpha;
                _icons.Add(bi.transform);

                GameObject bc = Instantiate(beaconCardPrefab);

                bc.transform.SetParent(_frontCanvas);
                bc.transform.localPosition = new Vector3(-270, 360 - _cards.Count * 70f, -1f);
                bc.transform.localScale = Vector3.one;
                bc.name = bd.minor.ToString();

                bc.transform.Find("Button").GetComponent<Button>().onClick.AddListener(BackToSetup);
                _cards.Add(bc.transform);

                BeaconWrapper newBW = new BeaconWrapper(bd.major, bd.minor, new Vector3(bd.x, bd.y, bd.z), bc, bi);

                _knownBeacons.Add(newBW);

                GameObject lineObj = new GameObject("line_" + i);
                lineObj.transform.parent = transform;
                lineRenderers.Add(NewLineRenderer(iconCol, 0.1f, lineObj));

                Color slrCol = new Color(colorvals[i,0], colorvals[i,1], colorvals[i,2], 1);

                GameObject slObj = new GameObject("signal_line_" + i);
                slObj.transform.parent = transform;
                signalLRs.Add(NewLineRenderer(slrCol, 0.01f, slObj));
            }
            else
            {
                Debug.Log("Can't get a proper BeaconDef from " + _iconstrings[i]);
            }
        }

    }

    // Create a new line renderer with the same (given) start and end colors and thickness, and add it as a component to /obj/.
    private LineRenderer NewLineRenderer(Color col, float thickness, GameObject obj)
    {
        LineRenderer lr = obj.AddComponent<LineRenderer>();
        lr.material = new Material(Shader.Find("Sprites/Default"));
        lr.startColor = col;
        lr.endColor = col;
        lr.startWidth = thickness;
        lr.endWidth = thickness;

        lr.sortingOrder = 0;
        lr.sortingLayerName = "top";
    
        return lr;
    }

    private void RenderGrid() {
        _pixels = new Transform[width, height];

        int startX = -width / 2;
        int startY = -height / 2;
        for (int x = startX; x < startX + width; x++) {
            for (int y = startY; y < startY + height; y++) {
                Transform pixel = Instantiate(square, new Vector3(x * PIXEL_SCALE, y * PIXEL_SCALE, 0), Quaternion.identity);
                pixel.localScale = Vector3.one * PIXEL_SCALE;
                pixel.parent = _frontCanvas;
                _pixels[x - startX, y - startY] = pixel;
            }
        }
    }


    private void FindDisplays()
    {
        // Detect the _icons
        foreach (BeaconWrapper b in _knownBeacons)
        {
            b.UpdateLight();

            List<OMobile.EstimoteUnity.EstimoteUnityBeacon> detecteds = _eud.getDetectedBeacons();
            foreach (OMobile.EstimoteUnity.EstimoteUnityBeacon db in detecteds)
            {
                if (db.Minor == b.minor)
                {
                    b.beacon = db;
                }
            }
        }
    }

    public void ToggleMode()
    {
        _mode = (_mode == 0) ? 1 : 0;
        GameObject.Find("mode text").GetComponent<Text>().text = _mode.ToString();

    }


    public void BackToSetup()
    {
        Debug.Log("BACK FROM WHENCE THOU CAMEST");

        SceneManager.LoadScene("Setup");
    }

    public void UpdateAverages(float[] dist_acc, float[] dist_rssi)
    {
       for(int i=0; i<_knownBeacons.Count; i++)
        {
            _readings_acc[_readingIndex, i] = dist_acc[i];
            _readings_rssi[_readingIndex, i] = dist_rssi[i];
        }
       
        _readingIndex++;
        if(_readingIndex >= NUM_READINGS)
        {
            _readingIndex = 0;
        }
    }

    public float AverageDistance(int beaconIndex)
    {
        float[,] readings = (_mode == 0) ? _readings_acc : _readings_rssi;
        float sum = 0;

        for(int i=0; i< NUM_READINGS; i++)
        {
            sum += readings[i, beaconIndex];
        }
        return sum / NUM_READINGS;
    }

    // Update is called once per frame
    void Update () {

        FindDisplays();

        FadeGrid();

        List<Vector3> bps = new List<Vector3>();
        List<Vector3> guessedPositions = new List<Vector3>();
            
        float[] accuracies = new float[_knownBeacons.Count];
        float[] distsfromrssi = new float[_knownBeacons.Count];

        if(_knownBeacons.Count >= 3 && _knownBeacons[0].beacon != null && _knownBeacons[1].beacon != null && _knownBeacons[2].beacon != null)
        {
            // ap : actual position
            Vector3 ap = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            ap.z = -0.1f;


            for (int i = 0; i<_knownBeacons.Count; i++)
            {
                accuracies[i] = (float)_knownBeacons[i].beacon.Accuracy;
                distsfromrssi[i] = EstimateDistance(_knownBeacons[i].beacon);
                bps.Add(UnZero(_icons[i].position));
            }

            UpdateAverages(accuracies, distsfromrssi);

            List<int[]> triplets = Triplets(_knownBeacons.Count);

            for (int i = 0; i < triplets.Count; i++)
            {
                int a = triplets[i][0];
                int b = triplets[i][1];
                int c = triplets[i][2];

                float distA = AverageDistance(a);
                float distB = AverageDistance(b);
                float distC = AverageDistance(c);
                guessedPositions.Add(GuessPosition(distA, distB, distC, bps[a].x, bps[a].y, bps[b].x, bps[b].y, bps[c].x, bps[c].y));           
            }

            float xSum = 0;
            float ySum = 0;
            float zSum = 0;
            for (int i = 0; i < guessedPositions.Count; i++)
            {
                xSum += guessedPositions[i].x;
                ySum += guessedPositions[i].y;
                zSum += guessedPositions[i].z;
            }
            // plot our guess at the average of all triangulated positions.
            guessPt.position = new Vector3(xSum / guessedPositions.Count, ySum / guessedPositions.Count, zSum / guessedPositions.Count);
            HeatPixel(guessPt.position);


            for (int i = 0; i < lineRenderers.Count; i++)
            {
                //DrawLineBetween(ap, bp, lineRenderers[i]);
                float guessRadius, signalRadius;

                Vector3 bp = _icons[i].position;
                signalRadius = (_mode == 0) ? accuracies[i] : distsfromrssi[i];
                guessRadius = XYdist0nceBetween(bp, guessPt.position);
                
                DrawCircleAround(_icons[i].position, guessRadius, lineRenderers[i]);
                DrawCircleAround(_icons[i].position, signalRadius, signalLRs[i]);
            }

            //ShowIfOnTarget();
        }

        foreach(BeaconWrapper bw in _knownBeacons)
        {
            if (bw.beacon != null)
            {
                float proximity = (float)bw.beacon.Accuracy;
                System.TimeSpan sinceSeen = System.DateTime.Now - bw.beacon.LastSeen;

                bw.txtDisplay.transform.Find("Button").Find("DisplayText").GetComponent<Text>().text = bw.major + "  /  " + bw.minor + "   "+sinceSeen.Seconds+"\n" + proximity;
            }
        }
    }

    private void HeatPixel(Vector3 pos)
    {
        if(_pixels == null)
        {
            return;
        }
        int xPixel = (int)Math.Round(pos.x / PIXEL_SCALE + width/2);
        int yPixel = (int)Math.Round(pos.y / PIXEL_SCALE + width / 2);

        if (xPixel >= 0 && xPixel < _pixels.GetLength(0) && yPixel >= 0 && yPixel < _pixels.GetLength(1))
        {
            Color col = _pixels[xPixel, yPixel].transform.GetComponent<SpriteRenderer>().color;
            if(UnityEngine.Random.value < 0.5f)
            {
                col.r = Mathf.Min(col.r + 0.5f,1f);
            } else
            {
                col.g = Mathf.Min(col.g + 0.5f,1f);
            }
           
            _pixels[xPixel, yPixel].transform.GetComponent<SpriteRenderer>().color = col;
        }
    }

    private void ClearGrid()
    {
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                _pixels[x, y].transform.GetComponent<SpriteRenderer>().color = Color.black;
            }
        }
    }

    private void FadeGrid()
    {
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                Color col = _pixels[x, y].transform.GetComponent<SpriteRenderer>().color;
                col.r = Mathf.Max(0, col.r - 0.01f);
                col.g = Mathf.Max(0, col.g - 0.01f);
                _pixels[x, y].transform.GetComponent<SpriteRenderer>().color = col;
            }
        }
    }

    public void ShowIfOnTarget()
    {
        targetArea.gameObject.SetActive(XYdist0nceBetween(guessPt.position, targetArea.position) < 1.5f);
    }


    /*
     *  Check all the values of your vector and if any of them are zero, nudge them up a bit
     *  so you don't go dividing by zero or anything
     */
    Vector3 UnZero(Vector3 v) {
        float almostZero = 0.0000001f;
        Vector3 v2 = v;
        if(v2.x == 0) {
            v2.x = almostZero;
        }
        if (v2.y == 0) {
            v2.y = almostZero;
        }
        if (v2.z == 0) {
            v2.z = almostZero;
        }
        return v2;
    }

    float UnZero(float f) {
        float almostZero = 0.0000001f;
        if (f == 0) {
            return almostZero;
        }
        return f;
    }

    float XYdist0nceBetween(Vector3 pt1, Vector3 pt2) {
        float dist = Mathf.Sqrt((pt1.x - pt2.x) * (pt1.x - pt2.x) + (pt1.y - pt2.y) * (pt1.y - pt2.y));
        return dist;
    }

    void DrawLineBetween(Vector3 pt1, Vector3 pt2, LineRenderer lr) {
        Vector3[] pts = new Vector3[2];
        pts[0] = pt1;
        pts[1] = pt2;
        lr.SetPositions(pts);
    }

    void DrawCircleAround(Vector3 center, float radius, LineRenderer lr) {
        int segments = 100;
        float angle = 0f;
        lr.positionCount = segments+1;
        float x, y;
        for(int i=0; i<(segments+1); i++) {
            x = Mathf.Sin(Mathf.Deg2Rad * angle) * radius;
            y = Mathf.Cos(Mathf.Deg2Rad * angle) * radius;
            lr.SetPosition(i, new Vector3(center.x + x, center.y + y, -0.2f));
            angle += (360f / segments);
        }
    }

    Vector3 EstimatePointFromRadii() {
        Vector3 ep = new Vector3();
        //float errorSum = 
        return ep;
    }

    float MeanSquaredError() {
        float error = 0f;
        return error;
    }

    private float EstimateDistance(OMobile.EstimoteUnity.EstimoteUnityBeacon beacon) {   

        float txPower = -74f; // Manufacture set this power in the device
        if (beacon.RSSI == 0) {

            return -1; // if we cannot determine accuracy, return -1.

        }

        float ratio = beacon.RSSI * 1 / txPower;
        if (ratio < 1.0) {
            return Mathf.Pow(ratio, 10);

        } else {
            float accuracy = (0.89976f) * Mathf.Pow(ratio, 7.7095f) + 0.111f;
            return accuracy;
        }
    }

    private float Sqr(float n) {
        return Mathf.Pow(n, 2f);
    }

    private Vector3 GuessPosition(float dist0, float dist1, float dist2, float ax, float ay, float bx, float by, float cx, float cy) {
        float j, k, x, y;
        if (_icons.Count < 3) {
            Debug.Log("Error! Please add at least three _icons!");
            return Vector3.zero;
        }
        // Method one: how does this even work when I'm passing it circles that often don't intersect??
        k = (Sqr(ax) + Sqr(ay) - Sqr(bx) - Sqr(by) - Sqr(dist0) + Sqr(dist1)) / (2 * (ay - by)) - (Sqr(ax) + Sqr(ay) - Sqr(cx) - Sqr(cy) - Sqr(dist0) + Sqr(dist2)) / (2 * (ay - cy));
        j = (cx - ax) / (ay - cy) - (bx - ax) / (ay - by);
        x = k / j;
        y = ((bx - ax) / (ay - by)) * x + (Sqr(ax) + Sqr(ay) - Sqr(bx) - Sqr(by) - Sqr(dist0) + Sqr(dist1)) / (2 * (ay - by));

        return new Vector3(x, y, -0.01f);
    }

    private List<int[]> Triplets (int beaconCount)
    {
        List<int[]> triplets = new List<int[]>();
        for(int i=0; i< beaconCount; i++)
        {
            for(int j=i+1; j< beaconCount; j++)
            {
                for (int k = j + 1; k < beaconCount; k++)
                {
                    triplets.Add(new int[3]{ i, j, k });
                }
            }
        }

        return triplets;
    }

}
